# SurveyEngine

This is real time survey engine concept inspired by menti.com

Very plain but has the basic functionality.

1. Build by Maven
2. Execute the jetty server by the class src/test/java/Start
3. Runs on localhost:8080
4. a. In admin section chose a question and send it to the connected clients
4. b. In survey section wait to receive a question and enter rating 1-5

There are many restrictions as this is very early concept :-)
For example single admin is assumed but not required.
And let's not speak about gui...