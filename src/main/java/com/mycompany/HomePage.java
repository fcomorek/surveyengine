package com.mycompany;

import java.util.Optional;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.mycompany.pages.AdminPage;
import com.mycompany.pages.AnswerPage;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;
	
	private AjaxFallbackLink<String> surveyLink;
	private AjaxFallbackLink<String> adminLink;

	public HomePage(final PageParameters parameters) {
		super(parameters);

		//add(new Label("version", getApplication().getFrameworkSettings().getVersion()));

		surveyLink = new AjaxFallbackLink<String> ("survey_link") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
				throw new RestartResponseAtInterceptPageException (new AnswerPage());
				
			}
		};
		add (surveyLink);
		
		adminLink = new AjaxFallbackLink<String> ("admin_link") {
			private static final long serialVersionUID = 1L;

			public void onClick (Optional<AjaxRequestTarget> target) {
				throw new RestartResponseAtInterceptPageException (new AdminPage());
			}
		};
		add (adminLink);
	}
}
