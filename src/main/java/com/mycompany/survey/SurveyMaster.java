package com.mycompany.survey;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class SurveyMaster {
	private SortedMap<String, Set<Integer>> questions = new TreeMap<>();
	
	private static SurveyMaster instance;

	private SurveyMaster() {
	}
	
	public static SurveyMaster getInstance() {
		if(instance == null) {
			synchronized (SurveyMaster.class) {
				if(instance == null) {
					instance = new SurveyMaster();
				}
			}
		}
		return instance;
	}
	
	public void addQuestion(String question) {
		questions.put(question, new HashSet<>());
	}

	public synchronized void addAnswer(String question, Integer answer) {
		questions.get(question).add(answer);
	}
	
	public Double getAverage(String question) {
		int sum = 0;
		for(Integer answer : questions.get(question)) {
			sum += answer;
		}
		if(questions.get(question).size() > 0) 
			return ((double)sum/(double)questions.get(question).size());
		else
			return 0.0d;
	}
	
	public SortedMap<String, Double> getAverages(){
		SortedMap<String, Double> result = new TreeMap<>();
		for (String question : questions.keySet()) {
			result.put(question, getAverage(question));
		}
		return result;
	}
}
