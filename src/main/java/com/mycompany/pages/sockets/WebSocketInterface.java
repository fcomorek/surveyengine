package com.mycompany.pages.sockets;

import org.apache.wicket.protocol.ws.api.message.ConnectedMessage;

public interface WebSocketInterface {

	public void addClient (ConnectedMessage message, String myID);
	
	public void sendToAll (String question);
	
	public void sendToAdmin (String id, int answer);
}
