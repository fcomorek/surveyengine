package com.mycompany.pages.sockets;

import java.io.Serializable;

import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;

/**
 * POJO
 *
 */
public class QuestionMessage implements IWebSocketPushMessage, Serializable {
	private static final long serialVersionUID = 1L;
	
	// This message represents the question that is being sent out to the clients
	private final String message;
	
	public QuestionMessage(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return this.message;
	}

}
