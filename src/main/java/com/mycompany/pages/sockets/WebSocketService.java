package com.mycompany.pages.sockets;

import java.util.HashMap;
import java.util.Map;

import org.apache.wicket.protocol.ws.WebSocketSettings;
import org.apache.wicket.protocol.ws.api.WebSocketPushBroadcaster;
import org.apache.wicket.protocol.ws.api.message.ConnectedMessage;
import org.apache.wicket.protocol.ws.api.registry.IWebSocketConnectionRegistry;

public class WebSocketService implements WebSocketInterface {
	
	private Map<String, ConnectedMessage> connections = new HashMap<String, ConnectedMessage>();
	private WebSocketPushBroadcaster broadcaster;
	
	private static WebSocketService instance;
	
	private WebSocketService() {}
	
	public static WebSocketService getInstance() {
		if(instance == null) {
			instance = new WebSocketService();
		}
		return instance;
	}

	public void addClient(ConnectedMessage message, String myID) {
		ConnectedMessage conMsg = new ConnectedMessage (message.getApplication(), message.getSessionId(), message.getKey());
		if(myID == null)
			connections.put(message.getSessionId(), conMsg);
		else
			connections.put(myID, conMsg);
		
		if (null == broadcaster) {
			WebSocketSettings webSocketSettings = WebSocketSettings.Holder.get(message.getApplication());
			IWebSocketConnectionRegistry webSocketConnectionRegistry = webSocketSettings.getConnectionRegistry();
			broadcaster = new WebSocketPushBroadcaster (webSocketConnectionRegistry);
		}
	}

	@Override
	public void sendToAll(String question) {
		if (null != broadcaster) {
			QuestionMessage message = new QuestionMessage(question);
			broadcaster.broadcastAll(connections.values().iterator().next().getApplication(), message);
		} else {
			throw new RuntimeException("WebSockets can not send message");
		}
	}

	@Override
	public void sendToAdmin(String id, int answer) {
		if (null != broadcaster && connections.containsKey(id)) {
			AnswerMessage msg = new AnswerMessage(answer);
			broadcaster.broadcast(connections.get(id), msg);
		} else {
			throw new RuntimeException("WebSockets can not send message");
		}

	}

}
