package com.mycompany.pages.sockets;

import java.io.Serializable;

import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;

/**
 * POJO
 *
 */
public class AnswerMessage implements IWebSocketPushMessage, Serializable {
	private static final long serialVersionUID = 1L;
	
	private final int rating;
	
	public AnswerMessage(int rating) {
		this.rating = rating;
	}
	
	public int getRating() {
		return this.rating;
	}

}
