package com.mycompany.pages;

import java.util.Optional;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.ws.api.WebSocketBehavior;
import org.apache.wicket.protocol.ws.api.WebSocketRequestHandler;
import org.apache.wicket.protocol.ws.api.message.ConnectedMessage;
import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;

import com.mycompany.pages.sockets.QuestionMessage;
import com.mycompany.pages.sockets.WebSocketService;
import com.mycompany.survey.SurveyMaster;

public class AnswerPage extends BasePage {
	private static final long serialVersionUID = 1L;

	// wicket
	private Model<String> updateModel;
	private Label questionLabel;
	private WebMarkupContainer answerContainer;

	public AnswerPage() {
	}

	@Override
	protected void onInitialize() {
		initPage();
		super.onInitialize();
	}

	private void initPage() {
		addElements();
		addWebSocketUpdating();
	}

	private void addElements() {
		updateModel = new Model<String>(null);

		questionLabel = new Label("question", updateModel);
		questionLabel.setOutputMarkupId(true);
		add(questionLabel);
		
		addLinks();
	}
		
	private void addLinks() {

		answerContainer = new WebMarkupContainer("wrapper");
		answerContainer.setOutputMarkupId(true);
		answerContainer.add(new AttributeModifier("style", "visibility:hidden;"));
		
		AjaxFallbackLink<String> link_1 = new AjaxFallbackLink<String>("survey_link_1") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
				SurveyMaster.getInstance().addAnswer(updateModel.getObject(), 1);
				answerContainer.add(new AttributeModifier("style", "visibility:hidden;"));
				sendAnswer(1);
				target.get().add(answerContainer);
			}
		};
		answerContainer.add(link_1);

		AjaxFallbackLink<String> link_2 = new AjaxFallbackLink<String>("survey_link_2") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
//				SurveyMaster.getInstance().addAnswer(updateModel.getObject(), 2);
				answerContainer.add(new AttributeModifier("style", "visibility:hidden;"));
				sendAnswer(2);
				target.get().add(answerContainer);
			}
		};
		answerContainer.add(link_2);

		AjaxFallbackLink<String> link_3 = new AjaxFallbackLink<String>("survey_link_3") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
//				SurveyMaster.getInstance().addAnswer(updateModel.getObject(), 3);
				answerContainer.add(new AttributeModifier("style", "visibility:hidden;"));
				sendAnswer(3);
				target.get().add(answerContainer);
			}
		};
		answerContainer.add(link_3);

		AjaxFallbackLink<String> link_4 = new AjaxFallbackLink<String>("survey_link_4") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
//				SurveyMaster.getInstance().addAnswer(updateModel.getObject(), 4);
				answerContainer.add(new AttributeModifier("style", "visibility:hidden;"));
				sendAnswer(4);
				target.get().add(answerContainer);
			}
		};
		answerContainer.add(link_4);

		AjaxFallbackLink<String> link_5 = new AjaxFallbackLink<String>("survey_link_5") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
				answerContainer.add(new AttributeModifier("style", "visibility:hidden;"));
				sendAnswer(5);
				target.get().add(answerContainer);
			}
		};
		answerContainer.add(link_5);
		
		add(answerContainer);
	}

	private void addWebSocketUpdating() {
		add(new WebSocketBehavior() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onConnect(ConnectedMessage message) {
				super.onConnect(message);

				WebSocketService.getInstance().addClient(message, null);
			}

			@Override
			protected void onPush(WebSocketRequestHandler handler, IWebSocketPushMessage message) {
				super.onPush(handler, message);

				if (message instanceof QuestionMessage) {
					QuestionMessage msg = (QuestionMessage) message;
					updateModel.setObject(msg.getMessage());
					answerContainer.add(new AttributeModifier("style", "visibility:visible;"));
				}
				handler.add(answerContainer);
				handler.add(questionLabel);
			}
		});
	}
	
	private void sendAnswer(int res) {
		WebSocketService.getInstance().sendToAdmin("admin", res);
	}
}
