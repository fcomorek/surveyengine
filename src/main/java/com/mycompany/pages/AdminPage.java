package com.mycompany.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.ws.api.WebSocketBehavior;
import org.apache.wicket.protocol.ws.api.WebSocketRequestHandler;
import org.apache.wicket.protocol.ws.api.message.ConnectedMessage;
import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;

import com.mycompany.pages.sockets.AnswerMessage;
import com.mycompany.pages.sockets.WebSocketService;
import com.mycompany.survey.SurveyMaster;

public class AdminPage extends BasePage {
	private static final long serialVersionUID = 1L;

	private Model<String> questionModel;
	private WebMarkupContainer listContainer;
	
	private String previousQuestinon;

	public AdminPage() {
	}

	@Override
	protected void onInitialize() {
		initPage();
		super.onInitialize();
	}

	private void initPage() {
		addForm();
		addAnswers();
		addWebSocketUpdating();
	}

	private void addAnswers() {
		IModel<List<Map.Entry<String,Double>>> answers = new LoadableDetachableModel<List<Map.Entry<String,Double>>>() {
			private static final long serialVersionUID = 1L;

			protected List<Map.Entry<String,Double>> load() {
				return new ArrayList<Map.Entry<String,Double>>(SurveyMaster.getInstance().getAverages().entrySet());
			}
		};

		ListView<Map.Entry<String,Double>> questionsView = new ListView<Map.Entry<String,Double>>("question_list", answers) {
			private static final long serialVersionUID = 1L;

			protected void populateItem(final ListItem<Map.Entry<String,Double>> item) {
				if (item.getModelObject() instanceof Map.Entry) {
					Map.Entry<String, Double> entry = (Map.Entry<String, Double>)item.getModelObject();
					
					item.add(new Label("question", entry.getKey()));
					item.add(new Label("average", entry.getValue()));
				} else {
					// log unexpected
					System.out.println("Wrong instance of the item in questionsView");
				}
				
			}
		};

		listContainer = new WebMarkupContainer("theContainer");
		listContainer.setOutputMarkupId(true);
		listContainer.add(questionsView);
		add(listContainer);
	}

	private void addForm() {
		Form<String> form = new Form<String>("question_form");

		questionModel = new Model<String>(null);

		TextField<String> textField = new TextField<>("survey_question");
		textField.setModel(questionModel);
		textField.setOutputMarkupId(true);

		form.add(textField);

		AjaxSubmitLink submit = new AjaxSubmitLink("submit_question") {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				previousQuestinon = questionModel.getObject();
				SurveyMaster.getInstance().addQuestion(previousQuestinon);
				WebSocketService.getInstance().sendToAll(previousQuestinon);
				
				questionModel.setObject("");

				target.add(textField);
				target.add(listContainer);

				super.onSubmit(target);
			}

		};
		form.add(submit);

		add(form);
	}
	
	private void addWebSocketUpdating() {
		add(new WebSocketBehavior() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onConnect(ConnectedMessage message) {
				super.onConnect(message);

				WebSocketService.getInstance().addClient(message, "admin");
			}

			@Override
			protected void onPush(WebSocketRequestHandler handler, IWebSocketPushMessage message) {
				super.onPush(handler, message);

				if (message instanceof AnswerMessage) {
					AnswerMessage msg = (AnswerMessage) message;
					SurveyMaster.getInstance().addAnswer(previousQuestinon, msg.getRating());
					handler.add(listContainer);
				}
				
			}
		});
	}
}
